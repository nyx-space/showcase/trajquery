extern crate clap;
extern crate nyx_space as nyx;

use clap::{App, Arg};
use nyx::celestia::{Bodies, Cosm, LTCorr};
use nyx::time::Epoch;
use nyx::NyxError;
use std::convert::TryFrom;
use std::str::FromStr;

fn main() -> Result<(), NyxError> {
    let matches = App::new("TrajQuery")
        .version("0.1")
        .author("Chris Rabotin <crabotin@masten.aero>")
        .about("Queries the DE438s NASA NAIF file")
        .arg("-F, --frames 'Show the available frames and nothing else'")
        .arg("-e, --epoch=[Epoch] 'Date time of the query, e.g. `2021-01-14T00:31:55 UTC`'")
        .arg(
            "-f, --frame=[Frame] '`EME2000` in `Moon as seen in the Earth Mean Equator 2000 Frame'",
        )
        .arg("-t, --target=[object] '`Moon` in `Moon as seen from Earth`'")
        .arg(
            Arg::new("correction")
                .short('c')
                .about("light correction: none (default), lt (light-time), ab (abberation)")
                .default_value("none"),
        )
        .get_matches();

    let cosm = Cosm::try_de438()?;

    if matches.is_present("frames") {
        println!("{:?}", cosm.frames_get_names());
        return Ok(());
    }

    let epoch = match matches.value_of("epoch") {
        Some(e_str) => match Epoch::from_str(e_str) {
            Ok(e) => e,
            Err(e) => return Err(NyxError::LoadingError(e.to_string())),
        },
        None => {
            return Err(NyxError::LoadingError(
                "Please provide an epoch".to_string(),
            ));
        }
    };

    let frame = match matches.value_of("frame") {
        Some(frame_s) => cosm.try_frame(frame_s)?,
        None => {
            return Err(NyxError::LoadingError(
                "Please provide a `frame`".to_string(),
            ));
        }
    };

    let target = match matches.value_of("target") {
        Some(target_s) => Bodies::try_from(target_s.to_string())?,
        None => {
            return Err(NyxError::LoadingError(
                "Please provide a `target` body".to_string(),
            ));
        }
    };

    let correction = match matches
        .value_of("correction")
        .unwrap()
        .to_lowercase()
        .as_str()
    {
        "none" => LTCorr::None,
        "lt" => LTCorr::LightTime,
        "ab" => LTCorr::Abberation,
        _ => {
            return Err(NyxError::LoadingError(format!(
                "unknown correction: `{}`",
                matches.value_of("correction").unwrap()
            )))
        }
    };

    let state = cosm.celestial_state(target.ephem_path(), epoch, frame, correction);
    println!("{}\n{:o}", state, state);

    Ok(())
}
