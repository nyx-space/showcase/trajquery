# TrajQuery

Query a subset of the de438s ephemeris in the requested frame.

Available celestial bodies:

+ Sun
+ Solar system barycenter (`SSB`)
+ Mercury
+ Venus
+ Earth-Moon barycenter
+ Earth
+ Moon (you may also specify `Luna` instead of `Moon`)
+ Mars barycenter
+ Jupiter barycenter
+ Saturn barycenter
+ Uranus barycenter
+ Neptune barycenter

Available frames: "SSB J2000", "Sun J2000", "iau sun", "Mercury Barycenter J2000", "Venus Barycenter J2000", "iau venus", "Earth Barycenter J2000", "Earth J2000", "iau earth", "Moon J2000", "iau moon", "Mars Barycenter J2000", "iau mars", "Jupiter Barycenter J2000", "iau jupiter", "Saturn Barycenter J2000", "iau saturn", "Uranus Barycenter J2000", "iau uranus", "Neptune Barycenter J2000", "iau neptune", "Pluto Barycenter J2000"

## Examples
### Requesting the frames

```
cargo run -- -F                                                   
    Finished dev [unoptimized + debuginfo] target(s) in 0.04s
     Running `target/debug/trajquery -F`
["SSB J2000", "Sun J2000", "iau sun", "Mercury Barycenter J2000", "Venus Barycenter J2000", "iau venus", "Earth Barycenter J2000", "Earth J2000", "iau earth", "Moon J2000", "iau moon", "Mars Barycenter J2000", "iau mars", "Jupiter Barycenter J2000", "iau jupiter", "Saturn Barycenter J2000", "iau saturn", "Uranus Barycenter J2000", "iau uranus", "Neptune Barycenter J2000", "iau neptune", "Pluto Barycenter J2000"]

```

### Requesting different bodies

```
$ cargo run -- -e "2021-01-14T00:31:55 UTC" -f "IAU Venus" -t "Luna"
    Finished dev [unoptimized + debuginfo] target(s) in 0.04s
     Running `target/debug/trajquery -e '2021-01-14T00:31:55 UTC' -f 'IAU Venus' -t Luna`
[Venus IAU Fixed] 2021-01-14T00:32:32 TAI       position = [206341555.033966, -14035925.662302, 120769837.025124] km    velocity = [24.423511, 49.050929, -24.696704] km/s
[Venus IAU Fixed] 2021-01-14T00:32:32 TAI       sma = -89.928096 km     ecc = 2651152.744670    inc = 43.092831 deg     raan = 214.729871 deg   aop = 126.974128 deg    ta = 5.455325 deg
 $ cargo run -- -e "2021-01-14T00:31:55 UTC" -f "IAU Venus" -t "Moon"
    Finished dev [unoptimized + debuginfo] target(s) in 0.04s
     Running `target/debug/trajquery -e '2021-01-14T00:31:55 UTC' -f 'IAU Venus' -t Moon`
[Venus IAU Fixed] 2021-01-14T00:32:32 TAI       position = [206341555.033966, -14035925.662302, 120769837.025124] km    velocity = [24.423511, 49.050929, -24.696704] km/s
[Venus IAU Fixed] 2021-01-14T00:32:32 TAI       sma = -89.928096 km     ecc = 2651152.744670    inc = 43.092831 deg     raan = 214.729871 deg   aop = 126.974128 deg    ta = 5.455325 deg
 $ cargo run -- -e "2021-01-14T00:31:55 UTC" -f "EME2000" -t "Moon"
    Finished dev [unoptimized + debuginfo] target(s) in 0.04s
     Running `target/debug/trajquery -e '2021-01-14T00:31:55 UTC' -f EME2000 -t Moon`
[Earth J2000] 2021-01-14T00:32:32 TAI   position = [211625.082300, -276317.281438, -146096.332141] km   velocity = [0.888881, 0.529986, 0.153332] km/s
[Earth J2000] 2021-01-14T00:32:32 TAI   sma = 391753.845030 km  ecc = 0.060883  inc = 24.897712 deg     raan = 12.189189 deg    aop = 237.123556 deg    ta = 56.045526 deg
 $ cargo run -- -e "2022-11-30 06:24:00 UTC" -f "EME2000" -t "Moon"
    Finished dev [unoptimized + debuginfo] target(s) in 0.04s
     Running `target/debug/trajquery -e '2022-11-30 06:24:00 UTC' -f EME2000 -t Moon`
[Earth J2000] 2022-11-30T06:24:37 TAI   position = [331637.666720, -140503.484607, -95673.859943] km    velocity = [0.513460, 0.828813, 0.391519] km/s
[Earth J2000] 2022-11-30T06:24:37 TAI   sma = 384995.447003 km  ecc = 0.052659  inc = 27.495602 deg     raan = 7.727886 deg     aop = 271.244737 deg    ta = 54.970363 deg
 $ cargo run -- -e "2022-11-30 06:24:00 UTC" -f "Luna" -t "Earth"
    Finished dev [unoptimized + debuginfo] target(s) in 0.04s
     Running `target/debug/trajquery -e '2022-11-30 06:24:00 UTC' -f Luna -t Earth`
[Moon J2000] 2022-11-30T06:24:37 TAI    position = [-331637.666720, 140503.484607, 95673.859943] km     velocity = [-0.513460, -0.828813, -0.391519] km/s
[Moon J2000] 2022-11-30T06:24:37 TAI    sma = -4549.968147 km   ecc = 82.832180 inc = 27.495602 deg     raan = 7.727886 deg     aop = 143.789477 deg    ta = 2.425623 deg
 $ cargo run -- -e "2022-11-30 06:24:00 UTC" -f "Luna" -t "SSB"  
    Finished dev [unoptimized + debuginfo] target(s) in 0.04s
     Running `target/debug/trajquery -e '2022-11-30 06:24:00 UTC' -f Luna -t SSB`
[Moon J2000] 2022-11-30T06:24:37 TAI    position = [-54956949.072343, -125155899.223586, -54253329.736981] km   velocity = [27.536447, -11.071480, -4.831510] km/s
[Moon J2000] 2022-11-30T06:24:37 TAI    sma = -5.422390 km      ecc = 27108998.103429   inc = 23.457144 deg     raan = 0.131148 deg     aop = 246.193479 deg    ta = 1.742509 deg
 $ cargo run -- -e "2022-11-30 06:24:00 UTC" -f "Luna" -t "Mercury"
    Finished dev [unoptimized + debuginfo] target(s) in 0.04s
     Running `target/debug/trajquery -e '2022-11-30 06:24:00 UTC' -f Luna -t Mercury`
[Moon J2000] 2022-11-30T06:24:37 TAI    position = [-37102912.772848, -181413902.447042, -86267654.495240] km   velocity = [64.453257, 4.775411, -0.190770] km/s
[Moon J2000] 2022-11-30T06:24:37 TAI    sma = -1.173743 km      ecc = 168713885.681801  inc = 25.874016 deg     raan = 4.586060 deg     aop = 269.612455 deg    ta = 345.788427 deg
 $ cargo run -- -e "2022-11-30 06:24:00 UTC" -f "Luna" -t "Moon"   
    Finished dev [unoptimized + debuginfo] target(s) in 0.04s
     Running `target/debug/trajquery -e '2022-11-30 06:24:00 UTC' -f Luna -t Moon`
[Moon J2000] 2022-11-30T06:24:37 TAI    position = [0.000000, 0.000000, 0.000000] km    velocity = [0.000000, 0.000000, 0.000000] km/s
[Moon J2000] 2022-11-30T06:24:37 TAI    sma = 0.000000 km       ecc = NaN       inc = NaN deg   raan = 0.000000 deg     aop = 0.000000 deg      ta = 0.000000 deg
```